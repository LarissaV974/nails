<?php
require_once("Models/membres.php");
$titre = "Connexion Client";

$msg = "";

if (isset($_POST['submit'])) {
	$email = $_POST['email'];
	$mdp = $_POST['mdp'];
	if (!empty($email) and !empty($mdp)) {
		$membres = $this->managemembre->getMembres();
		foreach ($membres as $cle => $ligne) {
			$membres = new Membres($ligne);
			$row = $this->managemembre->getAllMembres() ;
			if($row > 1){
				if($email == $membres->getemail()){
					$verifymdp = $membres->getmdp();
					if($mdp == $verifymdp){
						session_start();
						$_SESSION['membres'] = $email;
						header('Location:index.php?action=Connecter');
					}
					else{
						$msg = "Le mot de passe est incorect";
					}
				}
				else{
					$msg = " L'email est incorect";
				}
			}
		}
	}
	else{
		$msg = "Veuillez remplir tous les champs";
	}
}


$contenu = '
		<div class="card bg-light">
<article class="card-body mx-auto" style="max-width: 400px;">
	<h4 class="card-title mt-3 text-center">Administration - Connexion</h4>
    <p style="color:red;">'.$msg.'</p>
<form method="POST" action="">
    <div class="form-group input-group">
		<div class="input-group-prepend">
		 </div>
         <input class="form-control" type="text" id="email" name="email" placeholder="Votre Email" class="form-control" >
    </div> <!-- form-group// -->
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		 </div>
         <input type="password" id="mdp" name="mdp" class="form-control" placeholder="Mot de passe"><br><br>
    </div> <!-- form-group// -->
    <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary btn-block"> Se connecter </button>
    </div> <!-- form-group// -->
</form>
</article>
</div> <!-- card.// -->

</div> ';


include "template.php";

?>
