<body>
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.js"></script>
    <!-- HEADER -->
    <header>
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <!--Ajout Image-->
            <a class="navbar-brand" href=""><img src="images/logo.png" width="80" /></a>
            <ul class="navbar-nav">
                <!--RESPONSIVE-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <li class="nav-item active"><a class="nav-link" href="index.php">Accueil </a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Produits</a>
                        <div class="dropdown-menu">
                            <?php
                            //Affichage des produits dynamique..
                            require_once("Models/produits.php");
                            $categorie = $this->manageproduits->getCategorie();
                            foreach ($categorie as $categories) {
                                echo "
                                                <option value ='" . $categories["idcat"] . "'>
                                            <a class='dropdown-item' href='index.php?action=produitcat&amp;idcat=" . $categories['idcat'] . "'>
                                        " . $categories["nomcat"] . "</a>";
                            }
                            ?>
                        </div>
                    <li class="nav-item active"><a class="nav-link" href="index.php?action=Inscription&amp;form_inscription">Inscription</a></li>
                    <li class="nav-item active"><a class="nav-link" href="index.php?action=ConnexionClient">Connexion Client</a></li>
                    <li class="nav-item active"><a class="nav-link" href="index.php?action=ConnexionAdmin">Connexion Admin</a></li>
                    <?php
                    session_start();
                    if (isset($_SESSION['membres'])) {
                        echo '<button type="button" class="btn btn-info btn-light" data-toggle="modal" data-target="#myModal">PROFIL</button>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title"><h3 class="alert alert-primary" role="alert">Bienvenue,' . $_SESSION['membres'] . '</h3></h4>
                              </div>
                              <div class="modal-body">
                              <a style = "text-align:right;" href="index.php?action=Connecter&amp;Deconnecter"><button class="btn btn-alert" type="button">Se Deconnecter</button></a>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        
                      </div>
                      ';
                    }

                    if (isset($_GET['Deconnecter'])) {
                        session_destroy();
                        session_commit();
                        header('Location:index.php?action=ConnexionClient');
                    }
                    ?>
                    </li>
                    <!--A faire barre de recherche-->
            </ul>
        </nav>
        </div>
    </header>

    <body id="contenu">
        <section>
            <?= $message = isset($message) ? $message : NULL; ?>
            <?= $contenu = isset($contenu) ? $contenu : NULL; ?>
            <?php $this->manageclient->fermerConnexion(); ?>
        </section>
        <script>
            function profil() {
                el = document.getElementById("profil");
                el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
            }
        </script>
        <style>
            #contenu {
                height: 2000px;
            }
        </style>
