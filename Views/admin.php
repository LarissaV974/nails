<?php
require_once("Models/produits.php");
require_once("Models/membres.php");
session_start();
$titre = "Partie Administration ";
if (isset($_SESSION['login'])) {
  $contenu = '<div class="card bg-light">
    <h3 class="alert alert-primary" role="alert">Bienvenue,' . $_SESSION['login'] . '</h3>
    <a style = "text-align:right;" href="index.php?action=Admin&amp;Deconnecter"><button class="btn btn-alert" type="button">Se Deconnecter</button></a>
    <nav class="navbar navbar-light bg-light">
  <form class="form-inline">
    <a href="index.php?action=Admin&amp;products"><button class="btn btn-outline-dark" type="button">Liste des produits</button></a>
    <a href="index.php?action=Admin&amp;clients"><button class="btn btn-outline-dark" type="button">Liste des clients</button></a>
    <a href="index.php?action=Admin&amp;Ajouter"><button class="btn btn-outline-dark" type="button">Ajouter un produit </button></a>
  </form>
</nav>
     ';
  if (isset($_GET['Deconnecter'])) {
    session_destroy();
    session_commit();
    header('index.php');
  }
  if (isset($_GET['products'])) {
    $produits = $this->manageproduits->getProduits();
    foreach ($produits as $cle => $ligne) {
      $produits = new Produits($ligne);
      $contenu .= '
        <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nom du produit</th>
            <th scope="col">Description</th>
            <th scope="col">Prix</th>
            <th scope="col">Photo</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">' . $produits->getId() . '</th>
            <td>' . $produits->getName() . '</td>
            <td>' . $produits->getDescription() . '</td>
            <td>' . $produits->getPrice() . '</td>
            <td><img  width="50" src="' . $produits->getPhoto() . '"></td>
            <td><a href="index.php?action=Admin&amp;Modifier&amp;id=' . $produits->getId() . '"><button class="btn btn-outline-primary" type="button">Modifier</button></a>
            <a href="index.php?action=Admin&amp;Supprimer&amp;id=' . $produits->getId() . '"><button class="btn btn-outline-alert" type="button">Supprimer</button></a></td>
          </tr>
        </tbody>
      </table>';
    }
  } else {
    if (isset($_GET['Supprimer'])) {
      if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $this->manageproduits->Delete($id);
        header('Location:index.php?action=Admin&products');
        exit();
      }
    }
    if (isset($_GET['clients'])) {
      $titre = "Clients";
      $membres = $this->managemembre->getMembres();
      foreach ($membres as $cle => $ligne) {
        $membres = new Membres($ligne);
        $contenu .= '<table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Pseudo</th>
                <th scope="col">Mot de passe</th>
                <th scope="col">Email</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Adresse</th>
                <th scope="col">Ville</th>
                <th scope="col">Code Postal</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">' . $membres->getId() . '</th>
                <td>' . $membres->getPseudo() . '</td>
                <td>******</td>
                <td>' . $membres->getEmail() . '</td>
                <td>' . $membres->getNom() . '</td>
                <td>' . $membres->getPrenom() . '</td>
                <td>' . $membres->getAdresse() . '</td>
                <td>' . $membres->getVille() . '</td>
                <td>' . $membres->getCodePostal() . '</td>
              </tr>
            </tbody>
          </table>';
      }
    }
    if (isset($_GET['Ajouter'])) {
      $titre = "Ajouter";
      $msg = "";
      if (isset($_POST['form_add'])) {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $price = $_POST['price'];
        $photo = $_POST['photo'];
        $categorie = $_POST['categorie'];



        if (!empty($name) and !empty($description) and !empty($price) and !empty($photo) and !empty($categorie)) {
          $this->manageproduits->getAddProduits();
          header('Location:index.php?action=Admin&products');
        } else {
          $msg = "Veuillez remplir tout les champs<br>";
        }
      }
      $contenu .= '<div class="card bg-light">
      <article class="card-body mx-auto" style="max-width: 400px;">
        <h4 class="card-title mt-3 text-center">Ajouter un produit</h4>
        <p style="color:red;">' . $msg . '</p>
      <form method="POST" action="">
          <div class="form-group input-group">
            <div class="input-group-prepend">
           </div>
               <input type="text" class="form-control" id="name" name="name" placeholder="Nom du produit">
          </div> <!-- form-group// -->
          <div class="form-group input-group">
            <div class="input-group-prepend">
           </div>
               <textarea type="text" class="form-control" id="description" name="description" placeholder="Description"></textarea>
          </div> <!-- form-group// -->
          <div class="form-group input-group">
            <div class="input-group-prepend">
           </div>
           <select class="form-control" type="price" id="price" name="price" placeholder="price">

              <option>Prix</option>
              <option>5</option>
              <option>10</option>
              <option>15</option>
              <option>20</option>

            </select>
          </div> <!-- form-group// -->
          <div class="form-group input-group">
            <div class="input-group-prepend">
           </div>
               <div class="form-group input-group">
          <div class="input-group-prepend">
           </div>
           <div class="form-group input-group">
            <div class="input-group-prepend">
           </div>
           <input type="file" class="form-control-file" id="photo" name="photo">
          </div> <!-- form-group// -->
          <select class="form-control" type="categorie" id="categorie" name="categorie" placeholder="categorie">';

?>
<?php
      //Affichage des produits dynamique..
      require_once("Models/produits.php");
      $categorie = $this->manageproduits->getProduits();
      foreach ($categorie as $categories) {
        $contenu .= '
                <option>' . $categories["categorie"] . '</option>';
      }
      $contenu .= '</select>
          </div> <!-- form-group// -->
          <div class="form-group input-group">
            <div class="input-group-prepend">
           </div>
          <div class="form-group">
            <input style="text-align:center;" type="submit" name="form_add" class="btn btn-primary btn-block" value="Ajouter">
          </div> <!-- form-group// -->                                                                     
      </form>
      </article>
      </div> <!-- card.// -->
      
      </div> 
      ';
    }
  }
} else {
  $contenu = "<p style='color:red;'> Vous avez été déconnecter ! Veuillez vous connecter en tant qu'administateur</p>";
}
include "template.php";
?>