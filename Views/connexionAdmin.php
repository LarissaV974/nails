<?php

//require_once("Models/produits.php");
session_start();

$user= 'admin';
$passworddef = 'admin';
$msg = "";

if(isset($_POST['submit']))
{
	$login = $_POST['login'];
	$password = $_POST['password'];

	if (!empty($_POST['login'])&&(!empty($_POST['password'])))
	{
		if ($login == $user && $password == $passworddef)
		{
			$_SESSION['login'] = $login;
			header('Location:index.php?action=Admin');
		}
		else
		{
			$msg = "Identifiant ou mot de passe incorrectes<br> Veuillez réessayer !";

		}
	}
	else
	{
		$msg = "Veuillez remplir tous les champs";
	}
}
$titre = "Connexion Admin";
$contenu = '<div class="card bg-light">
<article class="card-body mx-auto" style="max-width: 400px;">
	<h4 class="card-title mt-3 text-center">Administration - Connexion</h4>
    <p style="color:red;">'.$msg.'</p>
<form method="POST" action="">
    <div class="form-group input-group">
		<div class="input-group-prepend">
		 </div>
         <input class="form-control" type="text" id="login" name="login" placeholder="Votre pseudo" class="form-control" >
    </div> <!-- form-group// -->
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		 </div>
         <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe"><br><br>
    </div> <!-- form-group// -->
    <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary btn-block"> Se connecter </button>
    </div> <!-- form-group// -->
</form>
</article>
</div> <!-- card.// -->

</div> 
';

include "template.php";
?>