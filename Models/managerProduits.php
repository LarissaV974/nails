<?php
require_once("db.php");
/* Accès à la table client de la base de données *************************/
class manageProduits extends Database
{

    // Colonne
    public $idcat;
    public $namecat;
    public $id;
    public $ref;
    public $name;
    public $description;
    public $categorie;

    // Connexion à la base de données
    public function __construct()
    {
        parent::__construct();
    }

    // Extraction des données des clients depuis la base de données.
    public function getProduits()
    {
        $sql = "SELECT * FROM produits";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute();
        $produits = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor(); // Achève le traitement de la requête
        return $produits;
    }
    public function getProduitsID($id)
    {
        $sql = "SELECT * FROM produits WHERE id= $id";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute(array($id));
        $produits = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor();
        return $produits;
    }
    public function getProductCat($idcat)
    {
        $sql = "SELECT * FROM produits WHERE categorie = $idcat ";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute(array($idcat));
        $produits = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor();
        return $produits;
    }

    public function getCategorie()
    {
        $sql = "SELECT * FROM categorie";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute();
        $categorie = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor(); // Achève le traitement de la requête
        return $categorie;
    }

    public function getProduitsreccent()
    {
        $sql = "SELECT * FROM produits ORDER BY id DESC LIMIT 0,4";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute();
        $prodrec = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor(); // Achève le traitement de la requête
        return $prodrec;
    }
    public function getAddProduits()
    {
        if (isset($_POST['form_add'])) {
            $name = $_POST['name'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $photo = $_POST['photo'];
            $categorie = $_POST['categorie'];
            $msg = "";

            $sql = "INSERT INTO produits VALUES('','','$name','$description','$categorie','$price','images/$photo')";
            $rqt = $this->cnx->prepare($sql);
            $rqt->execute();
            $produits = $rqt->fetchAll(PDO::FETCH_ASSOC);
            $rqt->closeCursor(); // Achève le traitement de la requête
            return $produits;
        }
    }
    public function Delete($id)
    {
        $sql = "DELETE FROM produits WHERE id = $id ";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute(array($id));
        $produits = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor();
        return $produits;
    }
    public function Update($id)
    {
        if (isset($_POST['update'])) {
            $name = $_POST['name'];
            $description = $_POST['description'];
            $price = $_POST['price'];

            $sql = "UPDATE produits SET name='$name',description='$description',price='$price' WHERE id = $id ";
            $rqt = $this->cnx->prepare($sql);
            $rqt->execute(array($id));
            $produits = $rqt->fetchAll(PDO::FETCH_ASSOC);
            $rqt->closeCursor();
            return $produits;
        }
    }
}
