<?php
require_once("db.php");
/* Accès à la table client de la base de données *************************/
class manageMembres extends Database
{

    // Colonne
    public $pseudo;
    public $mdp;
    public $nom;
    public $prenom;
    public $email;
    public $ville;
    public $codepostal;
    public $adresse;

    // Connexion à la base de données
    public function __construct()
    {
        parent::__construct();
    }

    public function getMembres()
    {
        $sql = "SELECT * FROM membres";
        $rqt = $this->cnx->prepare($sql);
        $rqt->execute();
        $membres = $rqt->fetchAll(PDO::FETCH_ASSOC);
        $rqt->closeCursor(); // Achève le traitement de la requête
        return $membres;
    }

    public function getAddMembres()
    {
        if (isset($_POST['form_inscription'])) {
            $pseudo = $_POST['pseudo'];
            $mdp = $_POST['mdp'];
           // $mdp1 = password_hash($mdp, PASSWORD_DEFAULT);
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $email = $_POST['email'];
            $ville = $_POST['ville'];
            $codepostal = $_POST['code_postal'];
            $adresse = $_POST['adresse'];
            $msg = "";

            $sql = "INSERT INTO membres VALUES('','$pseudo','$mdp','$nom','$prenom','$email','$ville','$codepostal','$adresse')";
            $rqt = $this->cnx->prepare($sql);
            $rqt->execute();
            $membres = $rqt->fetchAll(PDO::FETCH_ASSOC);
            $rqt->closeCursor(); // Achève le traitement de la requête
            return $membres;
        }
    }

    public function getAllMembres()
    {
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $mdp = $_POST['mdp'];

            $sql = "SELECT count(*) FROM membres WHERE email = $email, mdp = $mdp";
            $rqt = $this->cnx->prepare($sql);
            $row = $rqt->rowCount();
            $rqt->execute();
            $membres = $rqt->fetchAll(PDO::FETCH_ASSOC);
            $rqt->closeCursor(); // Achève le traitement de la requête
            return $membres;
        }
    }
    // $sql = "SELECT pseudo,mdp FROM membres WHERE pseudo = ?";
    // $rqt = $this->cnx->prepare($sql);
    // $rqt->execute();
    // $membres = $rqt->fetchAll(PDO::FETCH_ASSOC);
    // $row = $sql->rowCount();
    // $rqt->closeCursor(); // Achève le traitement de la requête
    // return $membres;

    // // Extraction des données des clients depuis la base de données.
    // public function getClients()
    // {
    //     $sql = "SELECT * FROM client;";
    //     $rqt = $this->cnx->prepare($sql);
    //     $rqt->execute();
    //     $clients = $rqt->fetchAll(PDO::FETCH_ASSOC);
    //     $rqt->closeCursor(); // Achève le traitement de la requête
    //     return $clients;
    // }

    // // Récupère les données d'un client déterminé par son id
    // public function getClient($id)
    // {
    //     $sql = "SELECT * FROM client WHERE id = ?;";
    //     $rqt = $this->cnx->prepare($sql);
    //     $rqt->execute(array($id));
    //     $client = $rqt->fetch();
    //     $rqt->closeCursor();
    //     return $client;
    // }

    // // Supprime les données d'un client déterminé par son id
    // public function deleteClient($id)
    // {
    //     $sql = "DELETE FROM client WHERE id = ?;";
    //     $rqt = $this->cnx->prepare($sql);
    //     $resultat = $rqt->execute(array($id));
    //     return $resultat;
    // }

    // // Ajoute un client
    // public function addClient(array $client)
    // {

    //     $sql = "INSERT INTO client(name, email, phone) VALUES(?,?,?)";
    //     $rqt = $this->cnx->prepare($sql);
    //     $resultat  = $rqt->execute(array($client[0], $client[1], $client[2]));
    //     return $resultat;
    // }

    // // Modifie un client
    // public function updateClient(array $client)
    // {
    //     var_dump($client);
    //     exit;
    //     $sql = "UPDATE client SET name = ?, email = ?, phone = ? WHERE id = ?";
    //     $rqt = $this->cnx->prepare($sql);
    //     $resultat  = $rqt->execute(array($client[0], $client[1], $client[2], $client[3]));
    //     return $resultat;
    // }
}
