<?php
    /* Accès à la base de données *************************/
    class Database {
// Définition des attributs
protected $host;
protected $dbname;
protected $user;
protected $passwd;
protected $port;
  
protected $cnx;

// Constructeur initialisation des données
public function __construct(){

  /*  $this->host   = "localhost";
    $this->dbname = "nailsworld";
    $this->user   = "root";
    $this->passwd = "";
    $this->cnx = null;*/
    
    $this->host   = "mysql-nailsworld.alwaysdata.net";
    $this->dbname = "nailsworld_db";
    $this->user   = "232260_adminnel";
    $this->passwd = "Nelsie97480";

    }

// Constructeur Méthode de connexion à la base de données
public function getConnection(){
    
    try{
        $this->cnx = new PDO("mysql:host=".$this->host.";dbname=".$this->dbname, $this->user, $this->passwd);
        $this->cnx->exec("set names utf8");
    }catch(PDOException $exception){
        echo "Impossible de vous connecter à la base de données : " . $exception->getMessage();
    }
    return $this->cnx;
}

// Execute une requête SQL paramétrée
protected function executeRequete($sql, $vars = null)
{
    // Exécution d'une requête préparée
    $rqt = $this->cnx->prepare($sql);
    $rqt->execute($vars);
    return $rqt;
}

// Méthode de déconnexion à la base de données
public function fermerConnexion() 
{
    $this->cnx = null;
}
    }
