<?php
Class Controller{
	public $manageclient;
	public $manageproduits;
	public $managemembre;

	public function __construct()
	{
		require_once('Models/managerClient.php');
		$this->manageclient = new managerClient();
		$this->manageclient->getConnection();

		require_once("Models/managerProduits.php");
		$this->manageproduits = new manageProduits();
		$this->manageproduits->getConnection();

		require_once("Models/managerMembres.php");
		$this->managemembre = new manageMembres();
		$this->managemembre->getConnection();
	}

	public function gere($action, $id = null){
		switch ($action) {
			case 'editer':
				break;
				case 'Produits':
				$produits = $this->manageproduits->getProduits();
				include 'Views/VueProduits.php';
				break;
/* 				case 'categorie':
				$prodcat = $this->manageproduits->getProduitCat($idcat);
				include 'Views/VueProduits.php';
				break; */
				case 'ConnexionAdmin':
					//$admin = $this->manageadmin->getAdmin();
					include 'Views/connexionAdmin.php';
				break;
				case 'ConnexionClient':
					//$admin = $this->manageadmin->getAdmin();
					include 'Views/connexionClient.php';
				break;
				case'Inscription':
					include 'Views/inscription.php';
				break;
				case'Admin':
					include 'Views/admin.php';
				break;
				case 'FicheProduits':
					include 'Views/VueProduits.php';
					break;
				case 'produitcat':
					include 'Views/ListProduits.php';
				break;
				case'Contact':
					include 'Views/contact.php';
				break;
			
			default:
			$categorie = $this->manageproduits->getCategorie();
				include 'Views/VueAccueil.php';
				break;
		}
	}
}
